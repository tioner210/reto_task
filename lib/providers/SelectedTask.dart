import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reto_task/models/Task.dart';
import 'package:reto_task/servicios/TaskServices.dart';

final taskSelected = StateProvider<Task?>((_) => null);
