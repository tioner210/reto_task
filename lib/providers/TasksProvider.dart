import 'package:reto_task/models/Task.dart';
import 'package:reto_task/servicios/TaskServices.dart';
import 'package:riverpod/riverpod.dart';

final tasksProvider = StateNotifierProvider<TasksProviderService, List<Task>>(
    (_) => TasksProviderService(TaskService()));

class TasksProviderService extends StateNotifier<List<Task>> {
  final ModelTaskServicesI modelTask;

  TasksProviderService(this.modelTask) : super([]) {
    load();
  }

  /*
  * Carga todas las tareas
  */
  Future load() async {
    final data = await modelTask.getTasks();
    if (data is List) {
      state = data.map((e) => Task.fromJson(e ?? {})).toList();
    }
  }

/*
  * caso de uso agregar tarea
  */
  Future addTask(Task task) async {
    final data = task.toJson();
    final dataTask = await modelTask.postTasks(data);

    // state = [Task.fromJson(dataTask), ...state];

    load();

    return;
  }

  /*
  * caso de uso actualizar tarea
  */
  Future updateTask(int id, Task task) async {
    final data = task.toJson();
    final dataTask = await modelTask.editTask(id, data);
    final ndata = Task.fromJson(dataTask);
    final index = state.indexWhere((element) => element.id == ndata.id);
    if (index > -1) {
      state = state..removeAt(index);
    }
    state = [ndata, ...state];

    return;
  }

  Future<bool> removeTask(int id) async {
    final res = await modelTask.remove(id);

    if (res) {
      return true;
    } else {
      return false;
    }
  }
}
