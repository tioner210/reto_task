import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reto_task/models/Task.dart';
import 'package:reto_task/providers/SelectedTask.dart';
import 'package:reto_task/servicios/TaskServices.dart';

final taskProvider = StateNotifierProvider<CargadorDeTareaSeleccionada, Task?>(
    (ref) => CargadorDeTareaSeleccionada(ref.watch(taskSelected)?.id));

class CargadorDeTareaSeleccionada extends StateNotifier<Task?> {
  int? id;
  CargadorDeTareaSeleccionada(int? _id) : super(null) {
    id = _id;
    cargar();
  }

  Future cargar() async {
    if (id == null) {
      state = null;
    } else {
      final data = await TaskService().getTask(id);
      state = Task.fromJson(data);
    }
  }
}
