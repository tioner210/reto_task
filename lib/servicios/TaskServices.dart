import 'dart:convert';

import 'package:http/http.dart' as http;

abstract class ModelTaskServicesI {
  Future<dynamic> getTasks();

  Future<Map<String, dynamic>> editTask(int id, Map<String, dynamic> json);

  Future<Map<String, dynamic>> postTasks(Map<String, dynamic> json);

  Future<bool> remove(int id);

  Future<Map<String, dynamic>> getTask(int? id);
}

/*
 *  define servicios de crud para la entidad Task
 */
class TaskService implements ModelTaskServicesI {
  final _token =
      "e864a0c9eda63181d7d65bc73e61e3dc6b74ef9b82f7049f1fc7d9fc8f29706025bd271d1ee1822b15d654a84e1a0997b973a46f923cc9977b3fcbb064179ecd";

  final _base = "https://ecsdevapi.nextline.mx/vdev/tasks-challenge";

  get _auth => {"Authorization": "Bearer $_token"};
  get _content_url_encode =>
      {"Content-Type": "application/x-www-form-urlencoded"};

  /*
  * Obtiene todas las tareas
  */
  @override
  Future<dynamic> getTasks() async {
    const endPoint = "/tasks";

    final url = Uri.parse(_base + endPoint);

    final response = await http.get(url, headers: {..._auth});

    final code = response.statusCode;

    if (code < 200 || code >= 400) {
      throw Exception(jsonDecode(response.body)["message"]);
    }
    final data = jsonDecode(response.body);

    return data;
  }

  /*
  * Agrega una Tarea
  *[json] un mapa con los datos de la tarea
  *
  *
  *
  */
  @override
  Future<Map<String, dynamic>> postTasks(Map<String, dynamic> json) async {
    const endPoint = "/tasks";

    final url = Uri.parse(_base + endPoint);

    final response = await http.post(
      url,
      headers: {
        ..._auth,
        ..._content_url_encode,
      },
      body: json,
    );

    final code = response.statusCode;

    if (code < 200 || code >= 400) {
      throw Exception(jsonDecode(response.body)["message"]);
    } else {
      final taskData = jsonDecode(response.body);
      if (taskData["task"] != null) {
        return taskData["task"];
      } else {
        throw Exception("No se pudo crear la tarea");
      }
    }
  }

  /*
  * Actualiza una Tarea
  *[json] un mapa con los datos a actualizar 
  *
  *
  *
  */
  @override
  Future<Map<String, dynamic>> editTask(
      int id, Map<String, dynamic> json) async {
    final endPoint = "/tasks/$id";

    final url = Uri.parse(_base + endPoint);

    final response = await http.put(
      url,
      headers: {
        ..._auth,
        ..._content_url_encode,
      },
      body: json,
    );

    final code = response.statusCode;

    if (code < 200 || code >= 400) {
      throw Exception(jsonDecode(response.body)["message"]);
    } else {
      final taskData = jsonDecode(response.body);
      if (taskData["task"] != null) {
        return taskData["task"];
      } else {
        throw Exception("No se pudo crear la tarea");
      }
    }
  }

  @override
  Future<bool> remove(int id) async {
    final endPoint = "/tasks/$id";

    final url = Uri.parse(_base + endPoint);

    final response = await http.delete(
      url,
      headers: {
        ..._auth,
        ..._content_url_encode,
      },
    );

    final code = response.statusCode;

    if (code < 200 || code >= 400) {
      throw Exception(jsonDecode(response.body)["message"]);
    } else {
      final taskData = jsonDecode(response.body);
      if (taskData["detail"] == "Éxito al eliminar la tarea") {
        return true;
      } else {
        return false;
      }
    }
  }

  @override
  Future<Map<String, dynamic>> getTask(int? id) async {
    final endPoint = "/tasks/$id";

    final url = Uri.parse(_base + endPoint);

    final response =
        await http.get(url, headers: {..._auth, ..._content_url_encode});

    final code = response.statusCode;

    if (code < 200 || code >= 400) {
      throw Exception(jsonDecode(response.body)["message"]);
    }
    final data = jsonDecode(response.body);

    return data[0];
  }
}
