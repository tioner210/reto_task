/*
*  enumera las propiedades de la entidad [Task]
*/
enum TaskPropeties {
  title,
  id,
  isCompleted,
  dueDate,
  comments,
  description,
  tags,
  token,
}

extension TaskPropertiesExtension on TaskPropeties {
  //define el nombre de la propiedad en la base de datos
  String get llave {
    switch (this) {
      case TaskPropeties.title:
        return "title";
      case TaskPropeties.id:
        return "id";

      case TaskPropeties.isCompleted:
        return "is_completed";

      case TaskPropeties.dueDate:
        return "due_date";
      case TaskPropeties.comments:
        return "comments";
      case TaskPropeties.description:
        return "description";

      case TaskPropeties.tags:
        return "tags";
      case TaskPropeties.token:
        return "token";
    }
  }
}

/*
  Clase que model la entidad Task
*/
class Task {
  String? title;

  int? id;

  int? isCompleted;

  String? dueDate;

  String? comments;

  String? description;

  String? tags;

  String? token;

  Task({
    this.isCompleted = 0,
  });

  /*
  * genera un mapa con las propiedades de la tarea
 */
  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    for (final prop in TaskPropeties.values) {
      final llave = prop.llave;

      switch (prop) {
        case TaskPropeties.title:
          json[llave] = title;

          break;
        case TaskPropeties.id:
          break;
        case TaskPropeties.isCompleted:
          json[llave] = isCompleted.toString();

          break;
        case TaskPropeties.dueDate:
          if (dueDate != null) {
            json[llave] = dueDate;
          }

          break;
        case TaskPropeties.comments:
          json[llave] = comments ?? "";

          break;
        case TaskPropeties.description:
          json[llave] = description ?? "";

          break;
        case TaskPropeties.tags:
          json[llave] = tags ?? "";

          break;
        case TaskPropeties.token:
          json[llave] = token ?? "";

          break;
      }
    }
    return json;
  }

  /*
      contruye un objeto [Task] mediante un Mapa
  */
  Task.fromJson(Map<String, dynamic> json) {
    for (final prop in TaskPropeties.values) {
      final llave = prop.llave;
      final value = json[llave];
      if (value == null) {
        continue;
      }
      switch (prop) {
        case TaskPropeties.title:
          title = value;
          break;
        case TaskPropeties.id:
          id = value;
          break;
        case TaskPropeties.isCompleted:
          isCompleted = int.tryParse(value.toString());

          break;
        case TaskPropeties.dueDate:
          dueDate = value;
          break;
        case TaskPropeties.comments:
          comments = value;
          break;
        case TaskPropeties.description:
          description = value;
          break;
        case TaskPropeties.tags:
          tags = value;

          break;
        case TaskPropeties.token:
          token = value;

          break;
      }
    }
  }

//genera una copia del modelo
  Task copy() {
    final task = Task();
    for (final prop in TaskPropeties.values) {
      switch (prop) {
        case TaskPropeties.title:
          task.title = title;
          break;
        case TaskPropeties.id:
          task.id = id;

          break;
        case TaskPropeties.isCompleted:
          task.isCompleted = isCompleted;

          break;
        case TaskPropeties.dueDate:
          task.dueDate = dueDate;

          break;
        case TaskPropeties.comments:
          task.comments = comments;

          break;
        case TaskPropeties.description:
          task.description = description;

          break;
        case TaskPropeties.tags:
          task.tags = tags;

          break;
        case TaskPropeties.token:
          task.token = token;

          break;
      }
    }
    return task;
  }
}
