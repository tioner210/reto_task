import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reto_task/models/Task.dart';
import 'package:reto_task/providers/SelectedTask.dart';
import 'package:reto_task/providers/TasksProvider.dart';
import 'package:reto_task/router/Page.dart';

/*
* widget que lista tareas
*/
class TaskList extends ConsumerStatefulWidget {
  TaskList({
    required this.tasks,
  });

  final List<Task> tasks;

  @override
  ConsumerState<TaskList> createState() => _TaskListState();
}

class _TaskListState extends ConsumerState<TaskList> {
  bool init = false;

  @override
  void initState() {
    super.initState();
    anima();
  }

  anima() {
    setState(() {
      init = false;
    });

    Timer(Duration(milliseconds: 500), () {
      setState(() {
        init = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        await ref.read(tasksProvider.notifier).load();
        anima();
      },
      child: ListView.separated(
        padding: EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).size.height * .01,
            horizontal: MediaQuery.of(context).size.width * .02),
        itemBuilder: (c, i) {
          final element = widget.tasks[i];

          return AnimatedContainer(
            curve: Curves.easeIn,
            color: !init
                ? Colors.transparent
                : element.isCompleted == 1
                    ? Colors.green.withOpacity(.2)
                    : Colors.red.withOpacity(.07),
            duration: Duration(milliseconds: 200 * i),
            transform: Transform.translate(
                    offset: !init
                        ? Offset(MediaQuery.of(context).size.width, 0)
                        : const Offset(0, 0))
                .transform,
            child: Dismissible(
              secondaryBackground: SizedBox(),
              background: Container(
                color: Colors.white,
                child: Row(
                  children: [
                    Icon(
                      Icons.delete,
                      color: Colors.red,
                    )
                  ],
                ),
              ),
              confirmDismiss: (direction) async {
                if (element.id == null) {
                  return false;
                }
                return await ref
                    .read(tasksProvider.notifier)
                    .removeTask(element.id!);
              },
              onDismissed: (o) {
                ref.read(tasksProvider.notifier).load();
              },
              key: ValueKey<Task>(element),
              child: ListTile(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(element.title ?? ""),
                  ],
                ),
                subtitle: Row(
                  children: [
                    Text(element.dueDate ?? ""),
                  ],
                ),
                trailing: SizedBox(
                  width: MediaQuery.of(context).size.width * .15,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          ref.read(taskSelected.notifier).state = element;
                          Navigator.pushNamed(context, Pages.editTask.path);
                        },
                        icon: const Icon(
                          Icons.edit,
                          color: Colors.blue,
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  ref.read(taskSelected.notifier).state = element;
                  Navigator.pushNamed(context, Pages.viewTask.path);
                },
              ),
            ),
          );
        },
        itemCount: widget.tasks.length,
        separatorBuilder: (context, index) => const SizedBox(
          height: 8,
        ),
      ),
    );
  }
}
