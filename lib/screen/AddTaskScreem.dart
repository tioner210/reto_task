import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reto_task/models/EstadoPantalla.dart';
import 'package:reto_task/models/Task.dart';
import 'package:reto_task/providers/TasksProvider.dart';

/*

*pantalla caso de uso agregar tarea
*/
class AddTaskScreem extends StatefulWidget {
  AddTaskScreem({Key? key}) : super(key: key);

  @override
  State<AddTaskScreem> createState() => _AddTaskScreemState();
}

class _AddTaskScreemState extends State<AddTaskScreem> {
  final _formKey = GlobalKey<FormState>();

  late Task _task;

  estado_pantalla _estadoPantalla = estado_pantalla.none;

  set cargando(bool cargando) {
    setState(() {
      if (cargando) {
        _estadoPantalla = estado_pantalla.loading;
      } else {
        _estadoPantalla = estado_pantalla.none;
      }
    });
  }

  @override
  void initState() {
    _task = Task();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Agregar Tarea"),
      ),
      body: Stack(
        children: [
          if (estado_pantalla.loading == _estadoPantalla)
            const Center(child: CircularProgressIndicator()),
          SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height * .05,
                horizontal: MediaQuery.of(context).size.width * .05),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    decoration: const InputDecoration(
                      hintText: "Titulo",
                      labelText: "Titulo",
                    ),
                    initialValue: _task.title,
                    textInputAction: TextInputAction.next,
                    validator: (v) =>
                        v?.isEmpty ?? true == true ? "Requerido" : null,
                    onChanged: (v) {
                      setState(() {
                        _task.title = v;
                      });
                    },
                  ),
                  const Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Completada"),
                      CupertinoSwitch(
                          value: _task.isCompleted == 1,
                          onChanged: (v) {
                            setState(() {
                              _task.isCompleted = v ? 1 : 0;
                            });
                          }),
                    ],
                  ),
                  const Divider(),
                  DateTimePicker(
                    type: DateTimePickerType.date,
                    dateMask: 'yyyy-MM-d',
                    firstDate: DateTime(2000),
                    lastDate: DateTime(2100),
                    dateLabelText: 'Date Time',
                    use24HourFormat: false,
                    locale: const Locale('es', 'ES'),
                    onChanged: (v) {
                      setState(() {
                        _task.dueDate = v;
                      });
                    },
                    decoration: const InputDecoration(
                      hintText: "Fecha",
                      labelText: "Fecha",
                    ),
                  ),
                  const Divider(),
                  TextFormField(
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                      hintText: "Comentarios",
                      labelText: "Comentarios",
                    ),
                    initialValue: _task.comments,
                    onChanged: (v) {
                      setState(() {
                        _task.comments = v;
                      });
                    },
                  ),
                  const Divider(),
                  TextFormField(
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                      hintText: "Descripcción",
                      labelText: "Descripcción",
                    ),
                    initialValue: _task.description,
                    onChanged: (v) {
                      setState(() {
                        _task.description = v;
                      });
                    },
                  ),
                  const Divider(),
                  TextFormField(
                    textInputAction: TextInputAction.done,
                    decoration: const InputDecoration(
                      hintText: "Tags",
                      labelText: "Tags",
                    ),
                    initialValue: _task.tags,
                    onChanged: (v) {
                      setState(() {
                        _task.tags = v;
                      });
                    },
                  ),
                  const Divider(),
                  Consumer(builder: (context, ref, child) {
                    return ElevatedButton(
                      onPressed: estado_pantalla.loading == _estadoPantalla
                          ? null
                          : () {
                              if (_formKey.currentState?.validate() ?? false) {
                                setState(() {
                                  _estadoPantalla = estado_pantalla.loading;
                                  ref
                                      .read(tasksProvider.notifier)
                                      .addTask(_task)
                                      .then((value) {
                                    Navigator.maybeOf(context)?.pop(context);
                                  }).onError((error, stackTrace) {
                                    cargando = false;
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        const SnackBar(content: Text("Error")));
                                  });
                                });
                              }
                            },
                      child: Text("Guardar"),
                    );
                  })
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
