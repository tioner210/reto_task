import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reto_task/models/Task.dart';
import 'package:reto_task/providers/SelectedTask.dart';
import 'package:reto_task/providers/TasksProvider.dart';
import 'package:reto_task/router/Page.dart';
import 'package:reto_task/screen/widgets/TaskList.dart';

class Home extends StatelessWidget {
  const Home({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(
    BuildContext context,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Consumer(builder: (context, ref, child) {
        return TaskList(
          tasks: ref.watch(tasksProvider),
        );
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, Pages.addTask.path);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
