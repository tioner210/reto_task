import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reto_task/providers/TaskProvider.dart';

/*

*pantalla caso de uso ver tarea
*/
class ViewTaskScreem extends ConsumerWidget {
  const ViewTaskScreem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
    final task = ref.watch(taskProvider);

    return Scaffold(
      appBar: AppBar(
        title: Text("Tarea ${task?.id ?? ""}"),
      ),
      body: Center(
        child: task == null
            ? CircularProgressIndicator()
            : Container(
                width: MediaQuery.of(context).size.width * .8,
                height: MediaQuery.of(context).size.height * .5,
                margin: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * .05,
                    horizontal: MediaQuery.of(context).size.width * .05),
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * .05,
                    horizontal: MediaQuery.of(context).size.width * .05),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 10,
                      color: Colors.black12,
                    )
                  ],
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("id"),
                        Text(task.id.toString()),
                      ],
                    ),
                    Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Titulo"),
                        Text(task.title ?? ""),
                      ],
                    ),
                    Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Descripcción"),
                        Text(task.description ?? ""),
                      ],
                    ),
                    Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        if (task.isCompleted == 1)
                          Text("Completada")
                        else
                          Text("No completada")
                      ],
                    ),
                    Divider(),
                    Text(task.comments ?? ""),
                    Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Tags"),
                        Text(task.tags ?? ""),
                      ],
                    ),
                    Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Fecha"),
                        Text(task.dueDate ?? ""),
                      ],
                    ),
                    Divider(),
                  ],
                ),
              ),
      ),
    );
  }
}
