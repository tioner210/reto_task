import 'package:flutter/cupertino.dart';

import 'package:reto_task/screen/AddTaskScreem.dart';
import 'package:reto_task/screen/EditTaskScreem.dart';
import 'package:reto_task/screen/Home.dart';
import 'package:reto_task/screen/ViewTaskScreem.dart';

//enum de paginas del sistema
enum Pages { home, addTask, editTask, viewTask }

extension PageExtension on Pages {
  /*
    nombre usado para enrutar la pagina
  */
  String get path {
    switch (this) {
      case Pages.home:
        return "home";
      case Pages.addTask:
        return "addTask";
      case Pages.editTask:
        return "editTask";
      case Pages.viewTask:
        return "viewTask";
    }
  }

  /*
    widget representativo de la pagina la pagina
  */

  Widget get widget {
    switch (this) {
      case Pages.home:
        return const Home(title: "Sistema de gestión de tareas");
      case Pages.addTask:
        return AddTaskScreem();
      case Pages.editTask:
        return EditTaskScreem();
      case Pages.viewTask:
        return ViewTaskScreem();
    }
  }
}
